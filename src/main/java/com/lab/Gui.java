package com.lab;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Gui extends JFrame implements ActionListener {

    Label path;
    Button buttonSubmit, buttonExit;
    TextField textField;

    public Gui() {
        System.setProperty("submit", "#16A91D");
        System.setProperty("exit", "#1690A9");

        Font colorFont = new Font("Serif", Font.BOLD, 15);
        path = new Label("Please enter full path of meetings.txt file, eg D:\\TelepathyLabs\\meetings.txt");
        path.setBounds(50, 50, 1000, 20);
        textField = new TextField();
        textField.setBounds(50, 75, 1000, 20);
        add(path);
        add(textField);

        buttonSubmit = new Button("SUBMIT");
        buttonSubmit.setBounds(50, 100, 300, 40);
        buttonSubmit.setBackground(Color.getColor("submit"));
        buttonSubmit.setForeground(Color.WHITE);
        buttonSubmit.setFont(colorFont);
        buttonSubmit.addActionListener(this);
        add(buttonSubmit);

        buttonExit = new Button("EXIT");
        buttonExit.setBounds(425, 100, 300, 40);
        buttonExit.setBackground(Color.getColor("exit"));
        buttonExit.setForeground(Color.WHITE);
        buttonExit.setFont(colorFont);
        buttonExit.addActionListener(this);
        add(buttonExit);

        setSize(1200, 400);
        setLayout(null);
        setLocationRelativeTo(null);
        setVisible(true);
        setTitle("TELEPATHY LABS");
        Image icon = Toolkit.getDefaultToolkit().getImage("C:\\LabSourceCode\\src\\main\\java\\com\\lab\\telepathyLabs.PNG");
        setIconImage(icon);
    }

    public void actionPerformed(ActionEvent e1) {


        if (e1.getActionCommand().equalsIgnoreCase("SUBMIT")) {

            System.out.println("  textField.getText(): " + textField.getText());
            if (textField.getText().equalsIgnoreCase("")) {
                System.out.println("Looks like missed to enter the path. Try Again.");
                JOptionPane.showMessageDialog(null, "Looks like you missed to enter the path. Try Again.");
            } else {
                Scanner s = null;
                try {
                    s = new Scanner(new File(textField.getText()));
                } catch (FileNotFoundException e) {
                    JOptionPane.showMessageDialog(null, "Path invalid. Try again");
                }
                ArrayList<String> list = new ArrayList<String>();
                while (true) {
                    assert s != null;
                    if (!s.hasNext()) break;
                    list.add(s.next());
                }
                s.close();
                System.out.println("list: "+list);
                int listLength = list.size();
                ArrayList<Integer> arraylistStart = new ArrayList<Integer>();
                ArrayList<Integer> arraylistEnd = new ArrayList<Integer>();

                try {
                    for (int i = 0; i < listLength; i++) {
                        String val = list.get(i);
                        String[] split = val.split("-");

                        arraylistStart.add(Integer.parseInt(split[0].replace(":", "")));
                        arraylistEnd.add(Integer.parseInt(split[1].replace(":", "")));
                    }
                } catch (Exception e) {
                    System.out.println("OUTPUT: Looks like your file is corrupted. Please double check data in your txt file.");
                    JOptionPane.showMessageDialog(null, "OUTPUT: Looks like your file is corrupted. Please double check data in your txt file.");
                    System.exit(1);
                }

                System.out.println("OUTPUT: Meeting rooms required: " + MinimumNumberOfMeetingRooms.minimumNumberOfMeetingRooms(arraylistStart, arraylistEnd));
                JOptionPane.showMessageDialog(null, "Rooms Required: " + MinimumNumberOfMeetingRooms.minimumNumberOfMeetingRooms(arraylistStart, arraylistEnd));
            }
        }
        else if (e1.getActionCommand().equalsIgnoreCase("EXIT")) {
                System.exit(1);
            }

        }
    }
