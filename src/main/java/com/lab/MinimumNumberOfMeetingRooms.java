package com.lab;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.util.logging.Logger;

public class MinimumNumberOfMeetingRooms {

//    Logger logger = new Logger("LOGGER");

    public static int minimumNumberOfMeetingRooms(ArrayList<Integer> start, ArrayList<Integer> end) {

        Collections.sort(start);
        Collections.sort(end);
//        System.out.println("start sorted: "+start);
//        System.out.println("end sorted:   "+end);
        int startLength = start.size();

        if (startLength==0){
            return 0;
        }
        int i = 1;
        int j = 0;

        int minMeetingRoomsRequired = 1;
        int noOfOngoingMeetings = 1;

        while (i < startLength && j < startLength) {
            if (end.get(j) > start.get(i)) {
                noOfOngoingMeetings++;
                if (minMeetingRoomsRequired < noOfOngoingMeetings) {
                    minMeetingRoomsRequired = noOfOngoingMeetings;
                }
                i++;
            } else {
                noOfOngoingMeetings--;
                j++;
            }
        }

        return minMeetingRoomsRequired;
    }

}
